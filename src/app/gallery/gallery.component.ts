import { Component} from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})

export class GalleryComponent {
  password = '1234';
  title = '';
  url = '';
  input = '';
  showForm = false;
  images = [
    {title: 'Image 1', url: 'https://avatars.mds.yandex.net/i?id=2dbbd40bce4b1fd7290c0fdcb5e2cf63-5288018-images-thumbs&n=13'},
    {title: 'Image 2', url: 'https://avatars.mds.yandex.net/i?id=181cd2f40ce10abf9694f24153ad5bba-5245909-images-thumbs&n=13'},
    {title: 'Image 3', url: 'https://avatars.mds.yandex.net/get-zen_doc/1219682/pub_5eaa7423102eee24419d5607_5eaa74d77e79087ec3668df9/scale_1200'},
  ];

  inputCheck(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.input = target.value;
  }

  checkPassword() {
    if (this.password === this.input) {
      this.showForm = !this.showForm;
    }
  }
  onTitleChange(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.title = target.value;
  }
  onUrlChange(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.url = target.value;
  }
  formIsEmpty() {
    return this.title === '' || this.url === '';
  }

  onAddImage(event: Event) {
    event.preventDefault();
    this.images.push({
      title: this.title,
      url:  this.url
    });

    this.title = '';
    this.url = '';
  }
}
