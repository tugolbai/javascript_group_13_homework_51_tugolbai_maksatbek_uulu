import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent {
  @Input() title = 'Image';
  @Input() url = 'https://pixy.org/src/477/4774988.jpg';

}
