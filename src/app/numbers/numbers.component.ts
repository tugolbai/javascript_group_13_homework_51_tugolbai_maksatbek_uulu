import { Component } from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent {
  numbers: number[] = [];

  constructor() {
    this.numbers = this.generateNumbers();
  }

  generateNumbers() {
    const random: number[] = [];

    for (let i = 0; i < 5; i++) {
      const randomNumber = Math.floor(Math.random() * (36 - 5 + 1)) + 5;
      if (!random.includes(randomNumber)) {
        random.push(randomNumber);
      } else {
        i--;
      }
    }
    random.sort(function(a, b) {
      return a - b;
    });
    return random;
  }

  generateNewNumbers() {
    this.numbers = this.generateNumbers();
  }
}
